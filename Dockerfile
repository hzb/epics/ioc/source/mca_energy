FROM python:3.9-slim

# Set the working directory in the container
WORKDIR /softIOC

# Add user softioc
RUN useradd -m softioc

# Change ownership of the directory to the non-root user
RUN chown -R softioc /softIOC

# Switch to the non-root user
USER softioc

# Copy the requirements file into the container at /app
COPY requirements.txt /softIOC/

# Install the required dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Copy the rest of the application code into the container at /app
COPY . /softIOC/

RUN mkdir /softIOC/autosave
RUN mv autosave.json autosave/

# Define the command to run the application
CMD ["python", "ioc.py"]