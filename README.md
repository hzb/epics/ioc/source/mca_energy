## Energy Waveform for SDD

This IOC monitors the values in the OAESE SDD IOC for the calibration and determines the required energy waveform for plotting

Additionally it exposes PV's that allow the user to specify the hi and low regions of each ROI in terms of energy

## How to run the IOC in the container?

```
docker run -it --network=host --restart always -v /path/to/local/app/autosave:/softIOC/autosave --name mca_energy_pythonioc your_image_name
```